import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

let instance = null
function render () {
  instance = new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app') // 这里是挂载到自己的html中的 基座会拿到这个挂载后的html将其插入进去
}

if (window.__POWERED_BY_QIANKUN__) {
  // eslint-disable-next-line
  __webpack_public_path__ = window.__INJECTED_PUBLIC_PATH_BY_QIANKUN__
}

if (!window.__POWERED_BY_QIANKUN__) {
  render()
}

export async function bootstrap (props) {
  console.log(props)
}
export async function mount (props) {
  render(props)
}
export async function unmount (props) {
  instance.$destroy()
}
